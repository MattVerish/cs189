import numpy as np
import scipy.io
import sklearn.utils
import scipy.special
import scipy.optimize
import skimage.measure
import skimage.transform
import skimage.filters
import skimage.morphology
import skimage.feature
import matplotlib.pyplot as plt

def TrainNeuralNet(Features, Labels, epochs, startRateV, startRateW):
    V = np.zeros((200, 785))
    W = np.zeros((26, 201))
    for i in range(200):
        for j in range(785):
            V[i, j] = np.random.normal(0, .01)

    for i in range(26):
        for j in range(201):
            W[i, j] = np.random.normal(0, .01)

    count = 0
    Losses = []
    for k in range(epochs):
        Features, Labels = sklearn.utils.shuffle(Features, Labels)
        VLearningRate = startRateV * np.power(.5, k)
        WLearningRate = startRateW * np.power(.5, k)
        for i in range(len(Features)):
            feature = Features[i]
            label = Labels[i]
            H = np.tanh(np.dot(V, feature))
            H = np.append(H, 1)
            Z = scipy.special.expit(np.dot(W, H))

            Y = np.zeros(26)
            Y[label - 1] = 1


            Losses.append(Loss(Y, Z))

            gradV = np.multiply(np.dot(W.T, Z - Y), 1-np.square(H))
            feature = np.reshape(feature, (785, 1))
            gradV = np.reshape(gradV, (201, 1))
            gradV = np.dot(gradV, feature.T)
            H = np.reshape(H, (201, 1))
            gradW = np.dot(np.reshape(Z - Y, (26, 1)), H.T)

            gradV = gradV[:-1]
            V = V - ((VLearningRate/(np.log(i + 2))) * gradV)
            W = W - ((WLearningRate/(np.log(i + 2))) * gradW)
    print(Losses)
    newLosses = []
    avg = 0
    for k in range(len(Losses)):
        if k %100 == 0 and k != 0:
            newLosses.append(avg/100)
            avg = 0
        avg += Losses[k]
    plt.plot(newLosses)
    plt.show()
    return V, W

def classify(data, V, W):
    labels = []
    for i in range(len(data)):
        H = np.tanh(np.dot(V, data[i]))
        H = np.append(H, 1)
        Y = scipy.special.expit(np.dot(W, H))
        label = np.argmax(Y) + 1
        labels.append(label)
    return labels

def scoreNet(data, labels, V, W):
    predicted = classify(data, V, W)
    correct = 0
    for i in range(len(predicted)):
        if predicted[i] == labels[i]:
            correct += 1
    return correct/len(predicted)

def deskew(img):
    img = np.array(img, dtype='uint8')
    img = img.reshape((28, 28))
    moments = skimage.measure.moments(img, order=2)
    centroid = [moments[0, 1] / moments[0, 0], moments[1, 0] / moments[0, 0]]
    center_moments = skimage.measure.moments_central(img, centroid[0], centroid[1], 2)

    u02 = center_moments[0, 2] / center_moments[0, 0]
    u11 = center_moments[1, 1] / center_moments[0, 0]

    skew = u11 / u02
    transform = skimage.transform.AffineTransform(np.asarray([[1, skew, -.5 * 28 * skew], [0, 1, 0], [0, 0, 1]]))
    newimage = skimage.transform.warp(img, transform)
    return newimage

def Loss(trueY, predictedY):
    return -1*sum(trueY[i] * np.log(predictedY[i]) + (1 - trueY[i]) * np.log(1 - predictedY[i]) for i in range(26))

data = scipy.io.loadmat("hw6_data_dist/letters_data.mat")['train_x']
labels = scipy.io.loadmat("hw6_data_dist/letters_data.mat")['train_y']


#for i in range(len(data)):
#    data[i] = deskew(data[i]).reshape((784))
#    for j in range(784):
#        if data[i][j] < 150:
#            data[i][j] = 0
#        else:
#            data[i][j] = 255
data = data/255


data, labels = sklearn.utils.shuffle(data, labels)

shape = data.shape
newData = np.zeros((shape[0], shape[1] + 1))
for i in range(len(data)):
    newData[i] = np.append(data[i], [1])

trainData = newData[:100000]
trainLabels = labels[:100000]

testData = newData[100000:]
testLabels = labels[100000:]


vals = [.01, .02, .03, .04, .05, .06, .07, .08, .09, .1, .11, .12, .13, .14, .15, .16, .17, .18, .19, .2]


#V, W = TrainNeuralNet(trainData, trainLabels, 1, .11, .11)
#print(scoreNet(testData, testLabels, V, W))
#maxScore = 0
#maxI = 0
#for i in vals:
#    V, W = TrainNeuralNet(trainData, trainLabels, 20, i, i)
#    score = scoreNet(testData, testLabels, V, W)
#    if score > maxScore:
#        maxScore = score
#        maxI = i
#    print(score, i)
#print(maxScore, maxI)
#V, W = TrainNeuralNet(trainData, trainLabels, 5, 1, .001)
#print(scoreNet(testData, testLabels, V, W))

