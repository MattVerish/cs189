import scipy.io
import numpy as np
import sklearn.preprocessing
import math

data = scipy.io.loadmat("hw4_wine_dist/data.mat")['X']
#data = sklearn.preprocessing.normalize(data)
y = scipy.io.loadmat("hw4_wine_dist/data.mat")['y']
trainData = data[:5000]
testData = data[5000:]
trainLabels = y[:5000]
testLabels = y[5000:]

newY = np.zeros(trainLabels.shape[0])
for i in range(trainLabels.shape[0]):
    newY[i] = trainLabels[i]

w = np.zeros(12)
EPSILON = .01
LAMBDA = .5

def get_s(w):
    input = trainData.dot(w)
    return scipy.special.expit(input)

def Loss(predicted, true):
    return -true * np.log(predicted) - (1-true)*np.log(1-predicted)

def Cost(values):
    sum = 0
    for elem in values:
        sum += Loss(classify)
get_s(w)
s = get_s(w)
y = y.T

for i in range(100):
    s = get_s(w)
    w = w + EPSILON*(trainData.T.dot(newY - s) - 2*LAMBDA*w)

correct = 0
total = 0

for i in range(testData.shape[0]):
    if scipy.special.expit(w.dot(testData[i])) > .5:
        if testLabels[i] == 1:
            correct += 1
    else:
        if testLabels[i] == 0:
            correct += 1
    total += 1


def classify(x):
    val = scipy.special.expit(w.dot(x))
    return val
    #if val >= .5:
    #    return 1
    #return 0

def Loss(predicted, trueVal):
    if predicted == 1:
        predicted = 1 - 1e-15
    else:
        predicted = 1e-15
    return -trueVal * np.log(predicted) - (1-trueVal)*np.log(1-predicted)

def Cost(values, labels):
    sum = 0
    for i in range(len(values)):
        sum += Loss(classify(values[i]), labels[i])
    return sum/len(values)

print(Cost(testData, testLabels))
print(correct/total)



