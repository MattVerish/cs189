from __future__ import division
import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import scipy.io
import collections
import cv2
import skimage.measure
import skimage.transform
import skimage.filters
import skimage.morphology
import skimage.feature
import math
from PIL import Image
import sklearn.preprocessing
import sklearn.discriminant_analysis
def problem_2():
    delta = 0.025
    x = np.arange(-3.0, 3.0, delta)
    y = np.arange(-2.0, 2.0, delta)
    X, Y = np.meshgrid(x, y)
    Z1 = mlab.bivariate_normal(X, Y, 1.0, 1.0, 0.0, 0.0)
    Z2 = mlab.bivariate_normal(X, Y, 1.5, 0.5, 1, 1)
    # difference of Gaussians
    Z = 10.0 * (Z2 - Z1)


    # Create a simple contour plot with labels using default colors.  The
    # inline argument to clabel will control whether the labels are draw
    # over the line segments of the contour, removing the lines beneath
    # the label
    plt.figure()
    CS = plt.contour(X, Y, Z)
    plt.clabel(CS, inline=1, fontsize=10)
    plt.title('Simplest default with labels')

    plt.show()


class Gaussian_classifier():
    cov_matrices = {}
    inv_matrices = {}
    det = {}
    avg_cov_matrix = None
    cov_inverse = None
    labels = None
    mean_matrices = {}
    C = 1.0

    def __init__(self, labels, C=1.0):
        self.C = C
        self.labels = labels

    def fit(self, data):
        sorted_data = collections.defaultdict(list)
        for elem in data:
            index = elem[-1]
            elem = self.preProcess(elem[:-1])
            sorted_data[index].append(elem)
        for index in self.labels:
            sorted_data[index] = np.asarray(sorted_data[index])
            self.mean_matrices[index] = np.empty(len(sorted_data[index][0]))
            self.cov_matrices[index] = np.cov(sorted_data[index].T)
            for k in range(len(sorted_data[index][0])):
                self.mean_matrices[index][k] = np.mean(sorted_data[index][:, k])
        self.avg_cov_matrix = sum(list(self.cov_matrices.values())) / len(self.labels)
        if np.linalg.det(self.avg_cov_matrix) == 0:
            self.avg_cov_matrix = self.avg_cov_matrix + + self.C * np.identity(self.avg_cov_matrix.shape[0])
            self.cov_inverse = np.linalg.inv(self.avg_cov_matrix)
        for index in self.labels:
            #if np.linalg.det(self.cov_matrices[index]) == 0:
            self.cov_matrices[index] = self.cov_matrices[index] + self.C * np.identity(self.cov_matrices[index].shape[0])
            self.inv_matrices[index] = np.linalg.inv(self.cov_matrices[index])
            self.det[index] = np.linalg.slogdet(self.cov_matrices[index])

    def LDA_classify(self, point):
        point = self.preProcess(point)
        def linear_fn(x, mean, cov):
            return np.dot(np.dot(mean.T, cov), x) - 0.5 * np.dot(np.dot(mean.T, cov), mean) + np.log(1.0 / 10.0)

        return max(self.labels, key=lambda x: linear_fn(point, self.mean_matrices[x], self.cov_inverse))

    def QDA_classify(self, point):
        point = self.preProcess(point)

        def descriminant_fn(x, mean, incov, det):
            det = np.real(det)[0]
            return (-0.5) * (x - mean).T.dot(incov).dot(x - mean) - 0.5 * det + np.log(1.0 / 10.0)

        return max(self.labels,
                   key=lambda x: descriminant_fn(point, self.mean_matrices[x], self.inv_matrices[x], self.det[x]))

    # Takes in a mnist image and 'deskews' it to try to improve performance. Probably can be improved, inspired by
    # opencv article and implemented by messing around with moments and equations found in wikipedia. Seems to work.
    def deskew(self, img):
        img = np.array(img, dtype='uint8')
        img = img.reshape((28, 28))
        moments = skimage.measure.moments(img, order=2)
        centroid = [moments[0, 1] / moments[0, 0], moments[1, 0] / moments[0, 0]]
        center_moments = skimage.measure.moments_central(img, centroid[0], centroid[1], 2)

        u02 = center_moments[0, 2] / center_moments[0, 0]
        u11 = center_moments[1, 1] / center_moments[0, 0]

        skew = u11 / u02
        transform = skimage.transform.AffineTransform(np.asarray([[1, skew, -.5 * 28 * skew], [0, 1, 0], [0, 0, 1]]))
        newimage = skimage.transform.warp(img, transform)
        return newimage

    def preProcess(self, point):
        deskewed = self.deskew(point)
        processed = deskewed.reshape((784, ))
        normalized = processed/np.linalg.norm(processed)
        normalized = normalized.reshape((28, 28))
        hog = skimage.feature.hog(normalized, pixels_per_cell=(4, 4))
        return hog/np.linalg.norm(hog)

    def score(self, data):
        correct = 0.0
        for elem in data:
            label = elem[-1]
            guess = self.LDA_classify(elem[:-1])
            if label == guess:
                correct += 1.0
        return correct / len(data)

    def scoreQDA(self, data):
        correct = 0.0
        for elem in data:
            label = elem[-1]
            guess = self.QDA_classify(elem[:-1])
            if label == guess:
                correct += 1.0
        return correct / len(data)


def problem_6():
    mnist_raw_data = scipy.io.loadmat('hw3_mnist_dist\\hw3_mnist_dist\\train.mat')['trainX']
    spam_raw_data = scipy.io.loadmat('hw3_spam_dist\\dist\spam_data.mat')
    spam_features = spam_raw_data['training_data']
    spam_labels = spam_raw_data['training_labels'][0]

    #spam_test = spam_raw_data['test_data']
    #spam_test = sklearn.preprocessing.normalize(spam_test)

    #print spam_features[1]
    #spam_features = sklearn.preprocessing.normalize(spam_features)
    #labeled = np.zeros((spam_features.shape[0], spam_features.shape[1] + 1))
    #classifier = Gaussian_classifier([0, 1])
    #for i in range(len(spam_features)):
    #    labeled[i] = np.append(spam_features[i], spam_labels[i])

    #labeled = np.asarray(labeled)
    #np.random.shuffle(labeled)
    #validation_set = labeled[:5000]
    #classifier.fit(labeled[5000:])
    #print classifier.scoreQDA(validation_set)


    #out = open('spam_output.csv', "w")
    #out.write("ID,Category\n")
    #for i in range(len(spam_test)):
    #   out.write("%d,%d\n" % (i, classifier.LDA_classify(spam_test[i])))
    #out.close()
    #print classifier.scoreLDA(validation_set)

    np.random.shuffle(mnist_raw_data)
    validation_set = mnist_raw_data[:10000]
    training_set = mnist_raw_data[10000:]
    classifier = sklearn.discriminant_analysis.QuadraticDiscriminantAnalysis()
    classifier.fit(training_set[:,:784], training_set[:,-1])
    print classifier.score(validation_set[:,:784], validation_set[:,-1])
    #classifier = Gaussian_classifier([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    #classifier.fit(mnist_raw_data[10000:])
    #print classifier.scoreLDA(validation_set)
    #scores = []

    #classifier = sklearn.discriminant_analysis.QuadraticDiscriminantAnalysis()
    #classifier.fit(mnist_raw_data[10000:][:-1], mnist_raw_data[10000][-1])


    #np.random.shuffle(mnist_raw_data)
    #validation_set = mnist_raw_data[:10000]
    #classifier = Gaussian_classifier([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], C=.001)
    #classifier.fit(mnist_raw_data[10000:])
    #print .001 + (.0001 * (i-7)), "LDA", classifier.score(validation_set)
    #print "QDA", classifier.scoreQDA(validation_set)

    #classifier = Gaussian_classifier([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], C=.001)
    #classifier.fit(mnist_raw_data)
    #mnist_test_data = scipy.io.loadmat('C:\\Users\\Matt\\cs189\\HW3\\hw3_mnist_dist\\hw3_mnist_dist\\test.mat')['testX']
    #out = open('mnist_output.csv', "w")
    #out.write("ID,Category\n")
    #for i in range(len(mnist_test_data)):
    #    out.write("%d,%d\n" % (i, classifier.QDA_classify(mnist_test_data[i])))
    #out.close()

problem_6()

mnist_raw_data = scipy.io.loadmat('hw3_mnist_dist\\hw3_mnist_dist\\train.mat')['trainX']

for i in range(100):
    pixels = np.array(mnist_raw_data[i][:-1], dtype='uint8')
    pixels = pixels.reshape((28, 28))
    label = mnist_raw_data[i][-1]
    plt.title(label)
    plt.imshow(pixels, cmap='gray')
    plt.show()

    filtered = skimage.filters.rank.mean_percentile(pixels, skimage.morphology.disk(5), p0 = .25, p1=.75)
    print filtered.shape
    plt.imshow(pixels, cmap='gray')
    plt.show()

#    img = np.array(pixels, dtype='uint8')
#    img = img.reshape((28, 28))
#    moments = skimage.measure.moments(img, order=2)
#    centroid = [moments[0, 1] / moments[0, 0], moments[1, 0] / moments[0, 0]]
#    center_moments = skimage.measure.moments_central(img, centroid[0], centroid[1], 2)

#    u02 = center_moments[0, 2] / center_moments[0, 0]
#    u11 = center_moments[1, 1] / center_moments[0, 0]

#   skew = u11 / u02
#    transform = skimage.transform.AffineTransform(np.asarray([[1, skew, -.5 * 28 * skew], [0, 1, 0], [0, 0, 1]]))
#    newimage = skimage.transform.warp(img, transform)
#    plt.imshow(newimage, cmap='gray')
#    plt.show()

