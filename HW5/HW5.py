import scipy.io
import numpy as np
import pandas as pd
from collections import defaultdict
import math
from sklearn import preprocessing
import itertools
import numbers
import sys
from collections import Counter
census_raw_data = pd.read_csv("hw5_census_dist/train_data.csv").as_matrix()
titanic_raw_data = pd.read_csv("hw5_titanic_dist/titanic_training.csv").as_matrix()
spam_raw_data = scipy.io.loadmat("hw5_spam_dist/dist/spam_data.mat")['training_data']
spam_labels = scipy.io.loadmat("hw5_spam_dist/dist/spam_data.mat")['training_labels']

#print(titanic_raw_data.type())

#v = preprocessing.LabelEncoder()
#titanic_processed_data = v.fit_transform(titanic_raw_data)
#print(titanic_processed_data[0])
class Node():
    rule, leftTree, rightTree, label = None, None, None, None
    def __init__(self, rule=None, leftTree=None, rightTree=None, label = None):
        self.rule = rule
        self.leftTree = leftTree
        self.rightTree = rightTree
        self.label = label

class DecisionTree():
    Tree = None
    def __init__(self, maxDepth):
        self.depth = maxDepth
        self.Tree = None

    def classify(self, samples):
        labels = []
        for elem in samples:
            node = self.Tree
            while node.label == None:
                if node.rule(elem):
                    node = node.rightTree
                else:
                    node = node.leftTree
            labels.append(node.label)
        return labels

    def train(self, set, numFeatures=0):
        if numFeatures == 0:
            numFeatures = len(set[0]) - 1
        features = np.random.choice(range(len(set[0]) - 1), numFeatures, False)
        self.Tree = self.GrowTree(set, 1, features)

    def GrowTree(self, set, depth=1, features = []):
        features = np.asarray(features)
        if len(features) == 0:
            features = [i for i in range(len(set))]
        if len(set) == 0:
            return Node(label = "?")
        percent, label = self.maxPercent(set)
        if percent > .95 or depth > self.depth:
            return Node(label = label)
        index, rule = self.bestSplit(set, features)
        LeftSet = []
        RightSet = []
        for elem in set:
            if rule(elem):
                RightSet.append(elem)
            else:
                LeftSet.append(elem)
        LeftSet, RightSet = np.asarray(LeftSet), np.asarray(RightSet)
        return Node(rule, self.GrowTree(LeftSet, depth=depth + 1), self.GrowTree(RightSet, depth = depth+1))

    #takes in a set of feature vectors, if they all belong
    #to the same class return true, false otherwise.
    def allSame(self, set):
        if len(set) <= 1:
            return True
        label = set[0][-1]
        for elem in set:
            if elem[-1] != label:
                return False
        return True
    def maxPercent(self, set):
        if len(set) == 0:
            return None
        fractions = defaultdict(lambda:0)
        sum1 = 0
        for elem in set:
            sum1 += 1
            fractions[elem[-1]] += 1
        return max(fractions.values()) / sum1, max(fractions.keys(), key = lambda x: fractions[x])
    #Iterates through all features, a decision rule which returns 0 if an element should be in
    # left subtree and 1 otherwise
    def entropy(self, set):
        fractions = defaultdict(lambda: 0)
        total = len(set)
        for elem in set:
            fractions[elem[-1]] += 1
        sum = 0
        for elem in fractions.keys():
            fractions[elem] = fractions[elem] / total
            sum += fractions[elem] * np.log2(fractions[elem])
        return -1 * sum

    def weighted_entropy(self, set1, set2):
        return (len(set1) * self.entropy(set1) + len(set2) * self.entropy(set2)) / (len(set1) + len(set2))

    def splits(self, input, maxLen=6):
        input = set(input)
        length = len(input)
        num = length // 2

        num = min(num, maxLen)
        sets = []
        for i in range(num):
            sets.append(itertools.combinations(input, i + 1))
        return sets

    def bestSplit(self, samples, features):
        features = np.asarray(features)
        bestSet = None
        bestType = None
        bestFeature = 0
        bestValue = 0
        lowestEntropy = sys.maxsize
        for i in range(len(samples[0]) - 2):
            if i not in features:
                continue
            if not isinstance(samples[0][i], str):
                features = samples[:, i]
                keys = samples[:, -1]
                zipped = sorted(zip(features, keys))
                k, j = 0, 1
                leftCounts = defaultdict(lambda: 0)
                rightCounts = defaultdict(lambda: 0)
                for elem in zipped:
                    rightCounts[elem[1]] += 1
                value = None
                rightCounts[zipped[0][1]] -= 1
                leftCounts[zipped[0][1]] += 1
                while j < len(zipped):
                    if zipped[k][0] == zipped[j][0]:
                        k += 1
                        j += 1
                        leftCounts[zipped[k][1]] += 1
                        rightCounts[zipped[k][1]] -= 1
                    else:
                        ent = self.entropy2(leftCounts, rightCounts)
                        if ent < lowestEntropy:
                            lowestEntropy = ent
                            bestFeature = i
                            bestValue = (zipped[j][0] + zipped[k][0]) / 2
                            bestType = "numeric"
                        k += 1
                        j += 1
                        leftCounts[zipped[k][1]] += 1
                        rightCounts[zipped[k][1]] -= 1
            else:
                sets = self.splits(samples[:, i])
                for elem in sets:
                    leftSet = []
                    rightSet = []
                    for sample in samples:
                        if sample[i] in elem:
                            leftSet.append(sample)
                        else:
                            rightSet.append(sample)
                    ent = self.weighted_entropy(leftSet, rightSet)
                    if ent < lowestEntropy:
                        lowestEntropy = ent
                        bestFeature = i
                        bestSet = leftSet
                        bestType = "qualitative"
        return bestFeature, self.makeRule(bestFeature, bestValue, bestSet, bestType)

    def makeRule(self, feature= 0, value = 0, set = None, type="numeric"):
        if type == "qualitative":
            return lambda x: x[feature] not in set
        else:
            return lambda x: x[feature] > value

    def entropy2(self, counts1, counts2):
        ent = 0
        total1 = sum(counts1.values())
        for elem in counts1.values():
            if elem <= 0 or total1 == 0 or elem == "?":
                continue
            fraction = elem / total1
            ent += (fraction * np.log2(fraction))
        ent1 = -1 * ent

        ent = 0
        total2 = sum(counts2.values())
        for elem in counts2.values():
            if elem <= 0 or total1 == 0 or elem == "?":
                continue
            fraction = elem / total2
            ent += (fraction * np.log2(fraction))
        ent2 = -1 * ent

        return (total1 * ent1 + total2 * ent2) / (total1 + total2)



class RandomForest():

    trees = []
    def __init__(self, numTrees, treeDepth):
        for i in range(numTrees):
            self.trees.append(DecisionTree(treeDepth))

    def train(self, set):
        numFeatures = len(set[0])
        d = int(np.ceil(np.sqrt(numFeatures)))
        for elem in self.trees:
            elem.train(set, d)

    def classify(self, samples):
        predictions = []
        for tree in self.trees:
            predictions.append(tree.classify(samples))
        predictions = np.asarray(predictions)
        labels = []
        for i in range(len(samples)):
            labels.append(self.maxElem(predictions[:,i]))
        return labels

    def maxElem(self, array):
        count = Counter(array)
        return count.most_common(1)[0][0]



