import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import collections
import scipy.misc

mnist_data = scipy.io.loadmat("hw7_data/mnist_data/images.mat")['images']
mnist_data = mnist_data.reshape(-1, 60000).T

mnist_data = mnist_data/255.0
print(mnist_data.shape)

def distance(a, b):
    return np.linalg.norm(a-b)

def getInitialCenters(k, data):
    randomPoints = np.random.choice(np.arange(0, len(data)), k)
    centers = []
    for i in randomPoints:
        centers.append(data[i])
    return np.asarray(centers)


def closestCenter(centers, point, previous):
    minK = previous
    mindistance = distance(point, centers[previous])

    for i in range(len(centers)):
        if i == previous:
            continue
        dist = distance(centers[i], point)
        if dist < mindistance:
            minK = i
            mindistance = dist
    return minK

def k_means(k, data):
    centers = getInitialCenters(k, data)

    labels = collections.defaultdict(list)
    for i in range(len(data)):
        label = closestCenter(centers, data[i], 0)
        labels[label].append(data[i])

    while(True):
        for i in range(k):
            centers[i] = np.mean(labels[i], axis=0)

        newLabels = collections.defaultdict(list)
        changes = 0
        for i in range(k):
            for j in range(len(labels[i])):
                elem = labels[i][j]
                newLabel = closestCenter(centers, elem, i)
                if not (newLabel == i):
                    changes += 1
                newLabels[newLabel].append(elem)
        print('changes', changes)
        labels = newLabels
        if changes == 0:
            break
    return centers

import skimage.io

im = skimage.io.imread("hw7_data/low-rank_data/face.jpg")
im2 = skimage.io.imread("hw7_data/low-rank_data/sky.jpg")
im3= skimage.io.imread("D:/Porn/Bibi Jones/0032.jpg")
plt.imshow(im2, cmap='gray')
plt.show()

plt.imshow(im3)
plt.show()
def k_approx(image, k):
    u, s, v = np.linalg.svd(image, full_matrices=False)
    S = np.zeros((np.diag(s).shape))
    S[:k, :k] = np.diag(s[:k])
    return np.dot(u, np.dot(S, v))



errors = []
for i in range(100):
    approx = k_approx(im2, i+1).astype('uint8')
    plt.imshow(approx, cmap='gray')
    plt.title(i+1)
    plt.show()
    errors.append(np.mean((im-approx)**2))
print(errors)

plt.plot(errors)
plt.show()