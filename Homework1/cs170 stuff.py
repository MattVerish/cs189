def minsearch(A):
    print A
    n = len(A)
    if n == 1:
        return A[0]
    if A[0] > A[n/2]:
        return minsearch(A[:n/2])
    return min(A[0], minsearch(A[n/2:]))

print minsearch([8, 9, 1, 2, 3, 4, 5, 6, 7])
