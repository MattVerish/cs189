import numpy as np
import matplotlib.pyplot as plt
import sklearn.utils
from sklearn import svm
from scipy.io import loadmat
import matplotlib.patches as mpatches
from sklearn import model_selection
def minsearch(A):
    print A
    n = len(A)
    if n == 1:
        return A[0]
    if A[0] > A[n/2]:
        return minsearch(A[:n/2 - 1])
    return min(A[0], minsearch(A[n/2:]))

print minsearch([8, 9, 1, 2, 3, 4, 5, 6, 7])
#Define some constants to avoid magic numbers
MNIST_VALIDATION_SIZE = 10000
CIFAR_VALIDATION_SIZE = 5000
MNIST_TRAINING_SIZES = [100, 200, 500, 1000, 5000, 10000]
CIFAR_TRAINING_SIZES = [100, 200, 500, 1000, 5000]

#load all the data sets into variables
mnist_raw_data = loadmat("hw01_data/mnist/train.mat")["trainX"]
cifar_raw_data = loadmat("hw01_data/cifar/train.mat")["trainX"]
spam_raw_data = loadmat("hw01_data/spam/spam_data")["training_data"]
spam_raw_labels = loadmat("hw01_data/spam/spam_data")["training_labels"][0]

#shuffle each dataset to ensure good sampling
np.random.shuffle(mnist_raw_data)
np.random.shuffle(cifar_raw_data)
shuffled_spam_data, shuffled_spam_labels = sklearn.utils.shuffle(spam_raw_data, spam_raw_labels)

#set size of the validation set for the spam data
SPAM_VALIDATION_SIZE = int(.2*len(spam_raw_data))
SPAM_TRAINING_SIZES = [100, 200, 500, 1000, 2000, len(spam_raw_data) - SPAM_VALIDATION_SIZE]

#split sets into training and validation sets. For MNIST and CIFAR data, seperate
#the labels, which are the last element of each array, from the features
mnist_validation_data = [temp[:-1] for temp in mnist_raw_data[:MNIST_VALIDATION_SIZE]]
mnist_validation_labels = [temp[-1] for temp in mnist_raw_data[:MNIST_VALIDATION_SIZE]]
mnist_training_data = [temp[:-1] for temp in mnist_raw_data[MNIST_VALIDATION_SIZE:]]
mnist_training_labels = [temp[-1] for temp in mnist_raw_data[MNIST_VALIDATION_SIZE:]]

cifar_validation_data = [temp[:-1] for temp in cifar_raw_data[:CIFAR_VALIDATION_SIZE]]
cifar_validation_labels = [temp[-1] for temp in cifar_raw_data[:CIFAR_VALIDATION_SIZE]]
cifar_training_data = [temp[:-1] for temp in cifar_raw_data[CIFAR_VALIDATION_SIZE:]]
cifar_training_labels = [temp[-1] for temp in cifar_raw_data[CIFAR_VALIDATION_SIZE:]]

spam_validation_data = shuffled_spam_data[:SPAM_VALIDATION_SIZE]
spam_validation_labels = shuffled_spam_labels[:SPAM_VALIDATION_SIZE]
spam_training_data = shuffled_spam_data[SPAM_VALIDATION_SIZE:]
spam_training_labels = shuffled_spam_labels[SPAM_VALIDATION_SIZE:]

#Create a linear SVM for each of the data sets
mnist_SVC = svm.SVC(kernel = 'linear')
cifar_SVC = svm.SVC(kernel = 'linear')
spam_SVC = svm.SVC(kernel = 'linear')

#Create some arrays to store our accuracies on both the training and validation sets for each dataset
mnist_validation_accuracy, mnist_training_accuracy, cifar_validation_accuracy, cifar_training_accuracy, spam_validation_accuracy, spam_training_accuracy = [], [], [], [], [], []

#Train the SVMs on subsets of increasing size and record the accuracy on both the subset trained on and the
#validation set created earlier. Record the accuracy in the created arrays for plotting.
for i in MNIST_TRAINING_SIZES:
    mnist_SVC.fit(mnist_training_data[:i], mnist_training_labels[:i])
    mnist_validation_accuracy.append(mnist_SVC.score(mnist_validation_data, mnist_validation_labels))
    mnist_training_accuracy.append(mnist_SVC.score(mnist_training_data[:i], mnist_training_labels[:i]))

for i in CIFAR_TRAINING_SIZES:
    cifar_SVC.fit(cifar_training_data[:i], cifar_training_labels[:i])
    cifar_validation_accuracy.append(cifar_SVC.score(cifar_validation_data, cifar_validation_labels))
    cifar_training_accuracy.append(cifar_SVC.score(cifar_training_data[:i], cifar_training_labels[:i]))

for i in SPAM_TRAINING_SIZES:
    spam_SVC.fit(spam_training_data[:i], spam_training_labels[:i])
    spam_validation_accuracy.append(spam_SVC.score(spam_validation_data, spam_validation_labels))
    spam_training_accuracy.append(spam_SVC.score(spam_training_data[:i], spam_training_labels[:i]))

#Create a plot of the training accuracy and validation accuracy versus the number of samples trained on.
plt.plot(MNIST_TRAINING_SIZES, mnist_training_accuracy, 'b', MNIST_TRAINING_SIZES, mnist_validation_accuracy, 'r')
blue_patch = mpatches.Patch(color='blue', label='Training accuracy')
red_patch = mpatches.Patch(color='red', label='Validation accuracy')
plt.title('MNIST Accuracy')
plt.xlabel('Training size')
plt.ylabel('Accuracy')
plt.legend(handles=[blue_patch, red_patch], loc='lower right')
plt.show()

plt.plot(CIFAR_TRAINING_SIZES, cifar_training_accuracy, 'b', CIFAR_TRAINING_SIZES, cifar_validation_accuracy, 'r')
blue_patch = mpatches.Patch(color='blue', label='Training accuracy')
red_patch = mpatches.Patch(color='red', label='Validation accuracy')
plt.legend(handles=[blue_patch, red_patch], loc='center right')
plt.title('CIFAR Accuracy')
plt.xlabel('Training size')
plt.ylabel('Accuracy')
plt.show()

plt.plot(SPAM_TRAINING_SIZES, spam_training_accuracy, 'b', SPAM_TRAINING_SIZES, spam_validation_accuracy, 'r')
blue_patch = mpatches.Patch(color='blue', label='Training accuracy')
red_patch = mpatches.Patch(color='red', label='Validation accuracy')
plt.legend(handles=[blue_patch, red_patch], loc='lower right')
plt.title('SPAM Accuracy')
plt.xlabel('Training size')
plt.ylabel('Accuracy')
plt.show()


#Hyper-parameter tuning. For this I vary the C value by a factor of 10 each iteration,
#ranging from 10^-7 to 10^7, and train the SVM on 10,000 samples. Then I test the accuracy
#on the validation set and see which C value performed best.
accuracies = []
for i in range(15):
    print i
    tempSVC = svm.SVC(C = 10**(i-7), kernel = 'linear')
    tempSVC.fit(mnist_training_data[:10000], mnist_training_labels[:10000])
    accuracies.append(tempSVC.score(mnist_validation_data, mnist_validation_labels))

plt.plot([(temp - 7) for temp in range(15)], accuracies, 'b')
blue_patch = mpatches.Patch(color='blue', label='Validation Accuracy')
plt.title('Hyper-parameter tuning')
plt.xlabel("C-value, as a power of 10")
plt.ylabel("Accuracy")
plt.legend(handles=[blue_patch], loc='upper right')
plt.show()
print np.argmax(accuracies)

#Cross Validation
accuracies = []
for i in range(30):
    spam_SVM = svm.SVC(kernel = 'linear', C = i + 1)
    accuracy = model_selection.cross_val_score(spam_SVM, shuffled_spam_data, shuffled_spam_labels, cv = 5)
    accuracies.append(sum(accuracy)/len(accuracy))

plt.plot([(temp + 1) for temp in range(30)], accuracies, 'b')
blue_patch = mpatches.Patch(color='blue', label='Accuracy')
plt.title('Hyper-parameter tuning K-Fold')
plt.xlabel("C-value")
plt.ylabel("Accuracy")
plt.legend(handles=[blue_patch], loc='lower right')
plt.show()
print np.argmax(accuracies)


#Create Kaggle files.
mnist_SVM = svm.SVC(kernel = 'linear', C = 10**(-6))
mnist_SVM.fit([temp[:-1] for temp in mnist_raw_data], [temp[-1] for temp in mnist_raw_data])
mnist_test_data = loadmat("hw01_data/mnist/test.mat")['testX']
out = open('mnist_output.csv', "w")
out.write("ID,Category\n")
predictions = mnist_SVM.predict(mnist_test_data)
for i in range(len(mnist_test_data)):
    out.write("%d,%d\n"%(i, predictions[i]))
out.close()


spam_SVM = svm.SVC(kernel = 'rbf', C = 1)
spam_SVM.fit(shuffled_spam_data, shuffled_spam_labels)
spam_test_data = loadmat("hw01_data/spam/spam_data")["test_data"]
out = open('spam_output.csv', "w")
out.write("ID,Category\n")
predictions = spam_SVM.predict(spam_test_data)
for i in range(len(spam_test_data)):
    out.write("%d,%d\n"%(i, predictions[i]))
out.close()


for i in range(20):
    spam_linear_SVM = svm.SVC(kernel='linear', C = 10 + i)
    spam_sigmoid_SVM = svm.SVC(kernel='sigmoid', C = 10 + i)
    spam_rbf_SVM = svm.SVC(C = 10 + i)
    spam_poly_SVM = svm.SVC(kernel='poly', C = 10 + i)

    spam_linear_SVM.fit(spam_training_data, spam_training_labels)
    spam_sigmoid_SVM.fit(spam_training_data, spam_training_labels)
    spam_rbf_SVM.fit(spam_training_data, spam_training_labels)
    spam_poly_SVM.fit(spam_training_data, spam_training_labels)

    print "linear", spam_linear_SVM.score(spam_validation_data, spam_validation_labels), i
    print "sigmoid", spam_sigmoid_SVM.score(spam_validation_data, spam_validation_labels), i
    print "rbf", spam_rbf_SVM.score(spam_validation_data, spam_validation_labels), i
    print "poly", spam_poly_SVM.score(spam_validation_data, spam_validation_labels), i